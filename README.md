# draig: database REST API interactive generator

## What is DRAIG
DRAIG is a JS (NodeJS, ExpressJS, [KnexJS](http://knexjs.org), [Objection.js](https://vincit.github.io/objection.js/)) DB-backed microservice code generator derived from the excellent [openapi-generator](https://github.com/OpenAPITools/openapi-generator) for [DbC](https://en.wikipedia.org/wiki/Design_by_contract) (Design by Contract or Contract Based Development). You provide or design a contract (API definition in YAML) and the generator generates, for you, the DB tables, a fake (seed) initial database and the glue code (side effects) for CRUD operations.

With some more work (i.e. if you provide KnexJS or Objection queries) you can have complex DB interactions exposed as microservices in minutes.

DRAIG works seamlessly with [draig-car](https://gitlab.com/robarago/draig-car), a CLI and REPL tool to design the contract and the queries, generate the code test the API and much more without leaving the REPL or using a simple and readline-powered CLI.

Under the hood, `draig-car` uses `docker` (only in available in some target platforms) to pull, create and manage the database instances required to deploy the API in MySQL (MariaDB), PostgreSQL or Oracle in a transparent fashion. The queries and DB operations are DB independent, so you don't have to worry (although you want it to) about hosts, ports, admin users, etc. You can, however, use `SQLite` as the DB provider for testing or production use (if you dare) without the need for a `docker` container.

## How to use
Run
```
> npm -g install draig-oag`
```

For development work on `draig` itself, clone this repo, cd to draig dir and then run:
```
> npm -g install
```

The local working copy of the repository will be then linked with your installation. Use it alone or with `draig-car`.

## Using as standalone generator

This is a new modified distribution of `openapi-generator`. So, once installed, you only have to use it like this:
```
> openapi-generator generate -g draig <rest of params>
```

## What's OpenAPI
The goal of OpenAPI is to define a standard, language-agnostic interface to
REST APIs which allows both humans and computers to discover and understand the
capabilities of the service without access to source code, documentation, or
through network traffic inspection.

When properly described with OpenAPI, a consumer can understand and interact
with the remote service with a minimal amount of implementation logic. Similar
to what interfaces have done for lower-level programming, OpenAPI removes the
guesswork in calling the service.

Check out [OpenAPI-Spec](https://github.com/OAI/OpenAPI-Specification) for
additional information about the OpenAPI project, including additional
libraries with support for other languages and more. 

## How do debug openapi-generator output?
The following additional debug options are available for all codegen targets:

```
# -DdebugOpenAPI prints the OpenAPI Specification as interpreted by the codegen
# -DdebugModels prints models passed to the template engine
# -DdebugOperations prints operations passed to the template engine
# -DdebugSupportingFiles prints additional data passed to the template engine
```
