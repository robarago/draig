/*
 * Copyright 2018 OpenAPI-Generator Contributors (https://openapi-generator.tech)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.abandos.codegen;

import static org.openapitools.codegen.utils.StringUtils.camelize;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.HashSet;

import java.nio.file.Files;

import org.openapitools.codegen.CliOption;
import org.openapitools.codegen.CodegenConfig;
import org.openapitools.codegen.CodegenModel;
import org.openapitools.codegen.CodegenProperty;
import org.openapitools.codegen.CodegenOperation;
import org.openapitools.codegen.CodegenParameter;
import org.openapitools.codegen.CodegenResponse;
import org.openapitools.codegen.CodegenType;
import org.openapitools.codegen.DefaultCodegen;
import org.openapitools.codegen.SupportingFile;
import org.openapitools.codegen.meta.GeneratorMetadata;
import org.openapitools.codegen.meta.Stability;
import org.openapitools.codegen.utils.ModelUtils;
import org.openapitools.codegen.utils.URLPathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.PathItem.HttpMethod;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Schema;

public class DraigGenerator extends DefaultCodegen implements CodegenConfig {
	private static final Logger LOGGER = LoggerFactory.getLogger(DraigGenerator.class);

	public static final String EXPORTED_NAME = "draig";
	public static final String SERVER_PORT = "serverPort";
	public static final String DB_CLIENT = "dbClient";
	public static final String DB_NAME = "dbName";
	public static final String DB_HOST = "dbHost";
	public static final String DB_USER_NAME = "dbUserName";
	public static final String DB_USER_PASSWORD = "dbUserPassword";
	public static final String TESTS_CLEAN = "testsClean";

	protected String apiVersion = "1.0.0";
	protected String defaultServerPort = "3001";
	protected String implFolder = "services";
	protected String projectName = "openapi-draig-server";
	protected String exportedName;

	public DraigGenerator() {
		super();
		generatorMetadata = GeneratorMetadata.newBuilder(generatorMetadata).stability(
								Stability.BETA).build();
		outputFolder = "generated-code/draig";
		embeddedTemplateDir = templateDir = "draig";
		setReservedWordsLowerCase(
			Arrays.asList("break", "case", "class", "catch", "const", "continue",
				"debugger", "default", "delete", "do", "else", "export",
				"extends", "finally", "for", "function", "if", "import", "in",
				"instanceof", "let", "new", "return", "super", "switch", "this",
				"throw", "try", "typeof", "var", "void", "while", "with", "yield"));
		additionalProperties.put("apiVersion", apiVersion);
		additionalProperties.put("implFolder", implFolder);
		// models (one only file): don't clear, needed for create_tables.js
		// modelTemplateFiles.clear();
		supportingFiles.add(new SupportingFile("models.mustache", "", "models.js"));
		supportingFiles.add(new SupportingFile("oamodel.mustache", "", "oamodel.js"));
		// root
		apiTemplateFiles.put("controller.mustache", ".js");
		apiTemplateFiles.put("service.mustache", ".js");
		apiTestTemplateFiles.put("service_test.mustache", ".js");
		supportingFiles.add(new SupportingFile("rootHooks.mustache", "test",
                         "rootHooks.js"));
		supportingFiles.add(new SupportingFile("openapi.mustache", "api",
											   "openapi.yaml"));
		supportingFiles.add(new SupportingFile("config.mustache", "", "config.js"));
		supportingFiles.add(new SupportingFile("expressServer.mustache", "",
											   "expressServer.js"));
		supportingFiles.add(new SupportingFile("index.mustache", "", "index.js"));
		supportingFiles.add(new SupportingFile("logger.mustache", "", "logger.js"));
		supportingFiles.add(new SupportingFile("eslintrc.mustache", "",
											   ".eslintrc.json"));
		supportingFiles.add(new SupportingFile("knexfile.mustache", "",
											   "knexfile.js"));
		// utils folder
		supportingFiles.add(
				new SupportingFile("utils" + File.separator + "openapiRouter.mustache",
								"utils", "openapiRouter.js"));
		supportingFiles.add(
				new SupportingFile("utils" + File.separator +
							   "orm.mustache", "utils", "orm.js"));
		// controllers folder
		supportingFiles.add(
			new SupportingFile("controllers" + File.separator + "index.mustache",
								"controllers", "index.js"));
		supportingFiles.add(
			new SupportingFile("controllers" + File.separator + "Controller.mustache",
							   "controllers", "Controller.js"));
		// service folder
		supportingFiles.add(new SupportingFile("services" + File.separator +
											   "index.mustache", "services", "index.js"));
		supportingFiles.add(new SupportingFile("services" + File.separator +
											   "Service.mustache", "services", "Service.js"));
		// migrations and seeds folder
		supportingFiles.add(new SupportingFile("migrations" + File.separator +
											   "01_create_tables.mustache", "migrations",
											   "01_create_tables.js.new"));
		supportingFiles.add(
			new SupportingFile("seeds" + File.separator + "01_seed_database.mustache",
							   "seeds", "01_seed_database.js"));
		// do not overwrite if the file is already present
		supportingFiles.add(new SupportingFile("package.mustache", "package.json"));
		supportingFiles.add(new SupportingFile("README.mustache", "README.md"));
		// views
		supportingFiles.add(
			new SupportingFile("authorize.mustache", "api", "authorize.html"));
		
		// CLI Options
		cliOptions.add(new CliOption(SERVER_PORT, "TCP port to listen on."));
		cliOptions.add(new CliOption(DB_CLIENT,
									 "Database client (knex client) for DB connection"));
		cliOptions.add(new CliOption(DB_NAME,
									 "Database name for connection (or filename for SQLite3)"));
		cliOptions.add(new CliOption(DB_HOST,
									 "Hostname or IP of database host to connect"));
		cliOptions.add(new CliOption(DB_USER_NAME,
									 "Database username for the API connection"));
		cliOptions.add(new CliOption(DB_USER_PASSWORD,
									 "Database password for the API connection"));
		cliOptions.add(new CliOption(TESTS_CLEAN,
									 "Clean test folder on every generation"));

		// remove _ from specialCharReplacements
		specialCharReplacements.remove("_");
	}

	@Override
	public String apiPackage() {
		return "controllers";
	}

	@Override
	public String apiTestFileFolder() {
		return outputFolder + File.separator + "test";
	}

	/**
	 * Configures the type of generator.
	 *
	 * @return the CodegenType for this generator
	 * @see org.openapitools.codegen.CodegenType
	 */
	@Override
	public CodegenType getTag() {
		return CodegenType.SERVER;
	}

	/**
	 * Configures a friendly name for the generator. This will be used by the
	 * generator to select the library with the -g flag.
	 *
	 * @return the friendly name for the generator
	 */
	@Override
	public String getName() {
		return "draig";
	}

	/**
	 * Returns human-friendly help for the generator. Provide the consumer with help
	 * tips, parameters here
	 *
	 * @return A string value for the help message
	 */
	@Override
	public String getHelp() {
		return "Generates a NodeJS Express ORM/ODM server (alpha). IMPORTANT: this generator may subject to breaking changes without further notice).";
	}

	@Override
	public String toApiName(String name) {
		if (name.length() == 0) {
			return "Default";
		}

		return camelize(name);
	}

	@Override
	public String toApiFilename(String name) {
		return toApiName(name) + "Controller";
	}

	@Override
	public String apiFilename(String templateName, String tag) {
		String result = super.apiFilename(templateName, tag);

		if (templateName.equals("service.mustache")) {
			String stringToMatch = File.separator + "controllers" + File.separator;
			String replacement = File.separator + implFolder + File.separator;
			result = result.replace(stringToMatch, replacement);
			stringToMatch = "Controller.js";
			replacement = "Service.js";
			result = result.replace(stringToMatch, replacement);
		}

		return result;
	}

	/*
	 * @Override protected String implFileFolder(String output) { return
	 * outputFolder + File.separator + output + File.separator +
	 * apiPackage().replace('.', File.separatorChar); }
	 */

	/**
	 * Escapes a reserved word as defined in the `reservedWords` array. Handle
	 * escaping those terms here. This logic is only called if a variable matches
	 * the reserved words
	 *
	 * @return the escaped term
	 */
	@Override
	public String escapeReservedWord(String name) {
		if (this.reservedWordsMappings().containsKey(name)) {
			return this.reservedWordsMappings().get(name);
		}

		return "_" + name;
	}

	/**
	 * Location to write api files. You can use the apiPackage() as defined when the
	 * class is instantiated
	 */
	@Override
	public String apiFileFolder() {
		return outputFolder + File.separator + apiPackage().replace('.',
				File.separatorChar);
	}

	public String getExportedName() {
		return exportedName;
	}

	public void setExportedName(String name) {
		exportedName = name;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, Object> postProcessOperationsWithModels(
		Map<String, Object> objs, List<Object> allModels) {
		Map<String, Object> objectMap = (Map<String, Object>) objs.get("operations");
		List<CodegenOperation> operations = (List<CodegenOperation>)
											objectMap.get("operation");
		ArrayList<String> generableSchemas = new ArrayList<String>();
		final Map<String, Schema> schemas = ModelUtils.getSchemas(this.openAPI);

		for (Map.Entry<String, Schema> entry : schemas.entrySet()) {
			Schema s = entry.getValue();
			Map extensions = s.getExtensions();

			if (extensions != null && extensions.containsKey("x-draig-tableName")) {
				generableSchemas.add(entry.getKey());
				if(extensions.containsKey("x-draig-optional"))
					generableSchemas.add(extensions.get("x-draig-optional").toString());
			}
		}

		List<Map<String, Object>> imports = (List<Map<String, Object>>)
											objs.get("imports");

		// Remove imports not marked for table or view generation
		if (null != imports) {
			for (Iterator<Map<String, Object>> it = imports.iterator(); it.hasNext();) {
				final Map<String, Object> i = it.next();

				if (!generableSchemas.contains(i.get("import"))) {
					it.remove();
				}
			}
		}

		for (CodegenOperation operation : operations) {
			operation.httpMethod = operation.httpMethod.toLowerCase(Locale.ROOT);
			List<CodegenParameter> params = operation.allParams;

			if (params != null && params.size() == 0) {
				operation.allParams = null;
			}

			List<CodegenResponse> responses = operation.responses;

			if (responses != null) {
				for (CodegenResponse resp : responses) {
					if ("0".equals(resp.code)) {
						resp.code = "default";
					}
				}
			}

			if (operation.examples != null && !operation.examples.isEmpty()) {
				// Leave application/json* items only
				for (Iterator<Map<String, String>> it = operation.examples.iterator();
						it.hasNext();) {
					final Map<String, String> example = it.next();
					final String contentType = example.get("contentType");

					if (contentType == null || !contentType.startsWith("application/json")) {
						it.remove();
					}
				}
			}
		}

		return objs;
	}

	private String getTableName(String complexType, Map<String, Object> objs) {
		Object o = objs.get(complexType);
		if(null == o) return null;
		Map<String, Object> m = (Map<String, Object>)o;
		Map<String, Object> models =
			(Map<String, Object>)((List<Object>)m.get("models")).get(0);
		CodegenModel cm = (CodegenModel)models.get("model");
		Map<String, Object> exts = cm.getVendorExtensions();
		if(null == exts) return null;

		return (String)exts.get("x-draig-tableName");
	}

	@Override 
	public Map<String, Object> postProcessAllModels(Map<String, Object> objs) {
		DepGraph<Object> dgraph = new DepGraph();

		for (Map.Entry<String, Object> o : objs.entrySet()) {
			dgraph.putNode(o.getKey(), o.getValue());
			Map<String, Object> m = (Map<String, Object>)o.getValue();
			Map<String, Object> models =
				(Map<String, Object>)((List<Object>)m.get("models")).get(0);
			CodegenModel cm = (CodegenModel)models.get("model");
			String fromTableName = getTableName(cm.getName(), objs);
			if(null == fromTableName) continue;
			// Create reverse topological sortable item to add depedencies
			for (CodegenProperty p : cm.getAllVars()) {
				String toTableName = getTableName(p.getComplexType(), objs);
				if(null != toTableName) {
					if(p.isModel) dgraph.addDep(cm.getName(), p.getComplexType());
					Map<String, Object> ve = p.getVendorExtensions();
					ve.put("x-draig-sch-fromTable", fromTableName);
					ve.put("x-draig-sch-toTable", toTableName);
				}
				// The id attribute (autoincrement, primary) is named `id' very often
				if(p.getName().equalsIgnoreCase("id")) {
					Map<String, Object> ve = p.getVendorExtensions();
					ve.put("x-draig-sch-type-autokey", true);
				}
			}
		}

		return dgraph.revTopoSort();
	}

	@SuppressWarnings("unchecked")
	private static List<Map<String, Object>> getOperations(
	Map<String, Object> objs) {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		Map<String, Object> apiInfo = (Map<String, Object>) objs.get("apiInfo");
		List<Map<String, Object>> apis = (List<Map<String, Object>>)
										 apiInfo.get("apis");

		for (Map<String, Object> api : apis) {
			result.add((Map<String, Object>) api.get("operations"));
		}

		return result;
	}

	private static List<Map<String, Object>> sortOperationsByPath(
	List<CodegenOperation> ops) {
		Multimap<String, CodegenOperation> opsByPath = ArrayListMultimap.create();

		for (CodegenOperation op : ops) {
			opsByPath.put(op.path, op);
		}

		List<Map<String, Object>> opsByPathList = new
		ArrayList<Map<String, Object>>();

		for (Entry<String, Collection<CodegenOperation>> entry :
				opsByPath.asMap().entrySet()) {
			Map<String, Object> opsByPathEntry = new HashMap<String, Object>();
			opsByPathList.add(opsByPathEntry);
			opsByPathEntry.put("path", entry.getKey());
			opsByPathEntry.put("operation", entry.getValue());
			List<CodegenOperation> operationsForThisPath = Lists.newArrayList(
						entry.getValue());
			//operationsForThisPath.get(operationsForThisPath.size() - 1).hasMore = false;

			if (opsByPathList.size() < opsByPath.asMap().size()) {
				opsByPathEntry.put("hasMore", "true");
			}
		}

		return opsByPathList;
	}

	@Override
	public void processOpts() {
		super.processOpts();

		if (additionalProperties.containsKey(EXPORTED_NAME)) {
			setExportedName((String) additionalProperties.get(EXPORTED_NAME));
		}

		if (additionalProperties.containsKey(DB_CLIENT)) {
			String dbClient = (String)additionalProperties.get(DB_CLIENT);
			additionalProperties.put("dbClient" + dbClient.substring(0,
									 1).toUpperCase() + dbClient.substring(1), true);
		}
	}

	@Override
	public void preprocessOpenAPI(OpenAPI openAPI) {
		URL url = URLPathUtils.getServerURL(openAPI, serverVariableOverrides());
//		String host = URLPathUtils.getProtocolAndHost(url);
		String port = URLPathUtils.getPort(url, defaultServerPort);
//		String basePath = url.getPath();

		if (additionalProperties.containsKey(TESTS_CLEAN)) {
			Boolean cleanTests = (Boolean)additionalProperties.get(TESTS_CLEAN);
			if(cleanTests) {
				LOGGER.warn("Cleaning ALL test files in 'test' folder!");
				try {
					Files.newDirectoryStream(
						java.nio.file.Paths.get(outputFolder + File.separator + "test"),
						p -> p.toString().endsWith("Test.js")
					).forEach(p -> {
						try {
							Files.delete(p);
							LOGGER.info("Test file '" + p.getFileName() + "' deleted.");
						}
						catch(IOException e) { }
					});
				} catch(IOException e) {
					LOGGER.error(e.toString());
				}
			}
		}

		if (additionalProperties.containsKey(SERVER_PORT)) {
			port = additionalProperties.get(SERVER_PORT).toString();
		}

		this.additionalProperties.put(SERVER_PORT, port);

		if (openAPI.getInfo() != null) {
			Info info = openAPI.getInfo();

			if (info.getTitle() != null) {
				// when info.title is defined, use it for projectName
				// used in package.json
				projectName = info.getTitle().replaceAll("[^a-zA-Z0-9]",
							  "-").replaceAll("^[-]*", "").replaceAll("[-]*$", "")
							  .replaceAll("[-]{2,}", "-").toLowerCase(Locale.ROOT);
				this.additionalProperties.put("projectName", projectName);
			}
		}

		// need vendor extensions
		Paths paths = openAPI.getPaths();

		if (paths != null) {
			for (String pathname : paths.keySet()) {
				PathItem path = paths.get(pathname);
				Map<HttpMethod, Operation> operationMap = path.readOperationsMap();

				if (operationMap != null) {
					for (HttpMethod method : operationMap.keySet()) {
						Operation operation = operationMap.get(method);
						String tag = "default";

						if (operation.getTags() != null && operation.getTags().size() > 0) {
							tag = toApiName(operation.getTags().get(0));
						}

						if (operation.getOperationId() == null) {
							operation.setOperationId(getOrGenerateOperationId(operation, pathname,
													 method.toString()));
						}

						// add x-openapi-router-controller
						if (operation.getExtensions() == null
								|| operation.getExtensions().get("x-openapi-router-controller") == null) {
							operation.addExtension("x-openapi-router-controller",
												   sanitizeTag(tag) + "Controller");
						}

						// add x-openapi-router-service
						if (operation.getExtensions() == null
								|| operation.getExtensions().get("x-openapi-router-service") == null) {
							operation.addExtension("x-openapi-router-service",
												   sanitizeTag(tag) + "Service");
						}

						// add x-draig-<httpMethod> if not present
						if (operation.getExtensions() == null
								|| (operation.getExtensions().get("x-draig-get") == null
								&& operation.getExtensions().get("x-draig-post") == null
								&& operation.getExtensions().get("x-draig-put") == null
								&& operation.getExtensions().get("x-draig-patch") == null
								&& operation.getExtensions().get("x-draig-delete") == null)) {
							operation.addExtension("x-draig-"
								+ method.toString().toLowerCase(), true);
						}
					}
				}
			}
		}
	}

	@Override
	public Map<String, Object> postProcessSupportingFileData(
		Map<String, Object> objs) {
		LOGGER.info("Draig: postProcessing supporting file data started");
		generateYAMLSpecFile(objs);

		for (Map<String, Object> operations : getOperations(objs)) {
			@SuppressWarnings("unchecked")
			List<CodegenOperation> ops = (List<CodegenOperation>)
										 operations.get("operation");
			List<Map<String, Object>> opsByPathList = sortOperationsByPath(ops);
			operations.put("operationsByPath", opsByPathList);
		}

		return super.postProcessSupportingFileData(objs);
	}

	@Override
	public String removeNonNameElementToCamelCase(String name) {
		return removeNonNameElementToCamelCase(name, "[-:;#]");
	}

	@Override
	public String escapeUnsafeCharacters(String input) {
		return input.replace("*/", "*_/").replace("/*", "/_*");
	}

	@Override
	public String escapeQuotationMark(String input) {
		// remove " to avoid code injection
		return input.replace("\"", "");
	}
}

class DepGraph<T> {
	public Map<String, T> nodeData;
	public Map<String, Set<String>> nodeDeps;

	public DepGraph() {
		this.nodeData = new HashMap<String, T>();
		this.nodeDeps = new HashMap<String, Set<String>>();
	}

	public void putNode(String index, T data) {
		nodeData.put(index, data);
		nodeDeps.put(index, new HashSet<String>());
	}

	public void addDep(String index, String dep) {
		Set<String> tdeps = nodeDeps.get(index);
		if(null == tdeps) return;
		tdeps.add(dep);
	}

	private void sort(String n, Map<String, T> list, Map<String, Boolean> visited) {
		if(visited.get(n) != null) return;
		for(String s: nodeDeps.get(n))
			sort(s, list, visited);
		visited.put(n, Boolean.TRUE);
		list.put(Integer.toString(list.size() + 1), nodeData.get(n));
	}

	public Map<String, T> revTopoSort() {
		Map<String, T> list = new HashMap<String, T>();
		Map<String, Boolean> visited = new HashMap<String, Boolean>();
		for(String k: nodeData.keySet())
			sort(k, list, visited);
		return list;
	}
}
